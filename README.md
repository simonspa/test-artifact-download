# Example: Fetch Artifact from other Project

This small example shows how to fetch a job artifact from another project and dispatch its content in the current CI pipeline as new artifact.

## Prerequisites

* Source Project ID: GitLab project ID of the source project, to be obtained from the project's overview page if GitLab, right below the repository title. Set this as `SOURCE_PROJECT_ID` in the `.gitlab-ci.yml`.
* Source Reference Name: Name of tag or branch in the source repository to fetch the job artifact for. Set this as `SOURCE_REF_NAME` in the `.gitlab-ci.yml`.
* Source Job Name: Name of the job the artifact should be fetched for. Set this as `SOURCE_JOB_NAME` in the `.gitlab-ci.yml`.
* Access token: An access token to the source repository is required. This can be obtained from your user account's settings page and has to be configured in the target repository (where this CI here runs) at Settings -> CI/CD -> Variables as `API_TOKEN`.

## Example

The example in this repository downloads the artifacts from the Allpix Squared repository's `mkdocs` branch and dispatches the content to a second job which simply lists all files.