#!/usr/bin/env python

import os
import json
import sys

# Check if we got token:
if len(sys.argv) != 5:
  print "Not enough arguments, looking for API_TOKEN, PROJECT_ID, REFERENCE_NAME, JOB_NAME"
  sys.exit(1)

api_token = sys.argv[1]
ci_project_id = sys.argv[2]
ref_name = sys.argv[3]
job_name = sys.argv[4]

print "Downloading artifact for reference %s and job %s" % (ref_name, job_name)
os.system("curl --location --header \"PRIVATE-TOKEN:%s\" \"https://gitlab.cern.ch/api/v4/projects/%s/jobs/artifacts/%s/download?job=%s\" -o artifact.zip" % (api_token, ci_project_id, ref_name, job_name))
os.system("unzip artifact.zip; rm artifact.zip")
